<?php
/**
 * 配置文件
 */
return array(
    "db_host"=>$GLOBALS["db_host"],
    "db_user"=>$GLOBALS["db_user"],
    "db_pwd"=>$GLOBALS["db_pass"],
    "db_name"=>$GLOBALS["db_name"],
    "db_quert_encoding"=>"utf8",
    "db_table_prefix"=>$GLOBALS["prefix"],
	"mongodb_host"=>"localhost:27017",
	"mongodb_dbname"=>"HMShop",
    "path_mode"=>1, 
    "sys_config"=>array(
       "sys_name"=>"CleverPHP框架演示",
       "sys_url"=>"http://localhost/cms"    
    ),
);
?>
