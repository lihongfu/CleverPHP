CleverPHP MVC，当前最新版本为1.1。

新版加入了30多个功能，完美支持PHP 7 + Mongodb3.x，支持HBASE等大数据平台，支持高性能的RPC并行开发。

CleverPHP MVC专为PHP + MongoDB而开发，以优雅的代码方式，实现最高效的编程。使用CleverPHP MVC，你可以以这样的方式对MongoDB进行CURD操作。

$where["purchase_id"]=(String)strip_tags($purch_id);
$where=array('$or' =>
		array(array('user_id'=>$this->user_id),
			array('session_id'=>(String)SESS_ID)),
		'$and'=>array($where)
);
$m_temp_procurement_product=M();
$m_temp_procurement_product->table_name="temp_procurement_product";
$m_temp_procurement_product->where($where)->count();


帮助文档请查看documents目录下的文件。作者博客:http://beauty-soft.net/blog/ceiba/



