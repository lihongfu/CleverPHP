<?php
// +----------------------------------------------------------------------
// | CleverPHP [ WE CAN DO IT JUST BeautySoft ]
// +----------------------------------------------------------------------
// | Copyright (c) 2012~2017 http://git.oschina.net/ceiba/CleverPHP All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: ceiba <ceiba_@126.com>
// +----------------------------------------------------------------------

class CacheMemcache{
	/**
	 * Memcache生成缓存
	 * @see ICache::Memcaches()
	 */
	protected $memcache;
	function __construct(){
		if ($this->clientMemcache()==false)
			die("Memcache连接失败");
	}
	//连接Memcache
	protected function clientMemcache(){
		$this->memcache= new Memcache;
		return $this->memcache->connect(C("memcache_host"), (int)C("memcache_prot"))?true:false;
	}
	public function setCache($key,$data,$expire=""){
		try {
			empty($expire)?$expire=C("memcache_expire"):$expire=$expire;
			return $this->memcache->set($key,$data,MEMCACHE_COMPRESSED,$expire);
		}catch (Exception $e){
			exit($e->getMessage());
		}
	}
	/**
	 * 获取缓存
	 * @see ICache::getMemcache()
	 */
	public function getCache($key){
		try {
			return $this->memcache->get($key,2);
		}catch (Exception $e){
			die($e->getMessage());
		}
	}
	/**
	 * 删除memcache缓存
	 * @see ICache::delMemcache()
	 */
	 public function delCache($key){
		try {
			return $this->memcache->delete($key,0);
		}catch (Exception $e){
			die($e->getMessage());
		}
	}
	
	
}
?>