<?php
// +----------------------------------------------------------------------
// | CleverPHP [ WE CAN DO IT JUST BeautySoft ]
// +----------------------------------------------------------------------
// | Copyright (c) 2012~2017 http://git.oschina.net/ceiba/CleverPHP All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: ceiba <ceiba_@126.com>
// +----------------------------------------------------------------------

	/**
	 * 基于缓存系统的Session中间件
	 * @author Administrator
	 *
	 */
	class SessionCache {
		private static  $lifetime=null;
		private static  $time = null;
		public static  function init(){
			if(!array_key_exists("session_lifetime", C())){
				self::$lifetime=ini_get('session.gc_maxlifetime');
			}
			self::$time=time();
		}
		public static  function start(){
			session_set_save_handler(
					array(__CLASS__, 'open'),
					array(__CLASS__, 'close'),
					array(__CLASS__, 'read'),
					array(__CLASS__, 'write'),
					array(__CLASS__, 'destroy'),
					array(__CLASS__, 'gc')
				);
			@session_start();
		}

	    /**
	     * 打开
	     * Enter description here ...
	     * @param unknown_type $path
	     * @param unknown_type $name
	     */
		public static  function open($path, $name){
			return true;
		}
        /**
         * 关闭
         * Enter description here ...
         */
		public static function close(){
			return true;
		}
        /**
         * 读取
         * Enter description here ...
         * @param unknown_type $PHPSESSID
         */
		public static function read($PHPSESSID){
			$session_drive=C("session_drive");
			$CacheObj=new Cache($session_drive);
			$CacheObj->cacheObj->cacheDir="temp/sessions";
			$out= $CacheObj->get($PHPSESSID);
			if($out===false || $out == null)
				return '';
			return $out;
		}
        /**
         * 写入
         * Enter description here ...
         * @param unknown_type $PHPSESSID
         * @param unknown_type $data
         */ 
		public static function write($PHPSESSID, $data){
			$session_drive=C("session_drive");
			$CacheObj=new Cache($session_drive);
			$CacheObj->cacheObj->cacheDir="temp/sessions";
			return $CacheObj->set($PHPSESSID,$data);

		}
        /**
         * 注消
         * Enter description here ...
         * @param unknown_type $PHPSESSID
         */
		public static function destroy($PHPSESSID){
			$session_drive=C("session_drive");
			$CacheObj=new Cache($session_drive);
			$CacheObj->cacheObj->cacheDir="temp/sessions";
			return $CacheObj->set($PHPSESSID,null);
		}
        /**
         * 清空
         * Enter description here ...
         * @param unknown_type $lifetime
         */
		public static function gc($lifetime){
			/* $expire = time () - $lifetime;
			$sql = "delete from `$this->_table where `created_time` < $expire";
			return mysql_query ( $sql, $this->_db_link ); */
			$session_drive=C("session_drive");
			$CacheObj=new Cache($session_drive);
			$CacheObj->cacheObj->cacheDir="temp/sessions";
			return $CacheObj->delOverdueCache($lifetime);
		}

		private static function session_key($PHPSESSID){
			return $PHPSESSID;
		}	
	}
    
?>
