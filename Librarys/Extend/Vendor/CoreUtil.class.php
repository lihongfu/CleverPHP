<?php
// +----------------------------------------------------------------------
// | CleverPHP [ WE CAN DO IT JUST BeautySoft ]
// +----------------------------------------------------------------------
// | Copyright (c) 2012~2017 http://git.oschina.net/ceiba/CleverPHP All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: ceiba <ceiba_@126.com>
// +----------------------------------------------------------------------

class CoreUtil{
	/**
	 * 包括目录，递归目录下所有PHP文件(排除excludeload_dir)
	 * @param unknown $dir
	 */
	static  function includes($dir,$exclude_sufx=".interfaces",$excludeload_dir="_excludeload"){

		if(is_dir($dir))
		{
			if ($dh = opendir($dir))
			{
				while (($file = readdir($dh)) !== false)
				{
					if (stripos($dir,"/")){
						$last_dir_ext=@array_pop(explode("/",$dir));
						if ($last_dir_ext && strtolower($last_dir_ext)==$excludeload_dir){
							break;
						}
					}
					
					if((is_dir($dir."/".$file)) && $file!="." && $file!="..")
	
					{
						self::includes($dir."/".$file."");
					}
					else
					{
						if($file!="." && $file!="..")
	
						{
							$files= $dir."/".$file;
							if (stripos($files,".php")){
								//echo "文件：".$files."\n";
								if (!empty($exclude_sufx)){
									if (!stripos($files,".interfaces")){
										require_once $files;
									}
								}else{
									require_once $files;
								}
								
							}
							
	
						}
	
					}
				}
				closedir($dh);
			}
		}
	}
	static function array_diffs($array1, $ext_array2){
		if (!$array1 || !$ext_array2) return array();
		$arr=array();
		
		foreach ($array1 as $k=>$v){
			if (!array_key_exists($k, $ext_array2)){
				//$ar[$k]=$v;
				$arr[$k]=$v;
			}
		}
		return $arr;
	}
	
	/**
	 * 获得IP
	 * Enter description here ...
	 */
	static function getIP(){
	
		static $realip = NULL;
	
		if ($realip !== NULL)
		{
			return $realip;
		}
	
		if (isset($_SERVER))
		{
			if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
			{
				$arr = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
	
				/* 取X-Forwarded-For中第一个非unknown的有效IP字符串 */
				foreach ($arr AS $ip)
				{
					$ip = trim($ip);
	
					if ($ip != 'unknown')
					{
						$realip = $ip;
	
						break;
					}
				}
			}
			elseif (isset($_SERVER['HTTP_CLIENT_IP']))
			{
				$realip = $_SERVER['HTTP_CLIENT_IP'];
			}
			else
			{
				if (isset($_SERVER['REMOTE_ADDR']))
				{
					$realip = $_SERVER['REMOTE_ADDR'];
				}
				else
				{
					$realip = '0.0.0.0';
				}
			}
		}
		else
		{
			if (getenv('HTTP_X_FORWARDED_FOR'))
			{
				$realip = getenv('HTTP_X_FORWARDED_FOR');
			}
			elseif (getenv('HTTP_CLIENT_IP'))
			{
				$realip = getenv('HTTP_CLIENT_IP');
			}
			else
			{
				$realip = getenv('REMOTE_ADDR');
			}
		}
	
		preg_match("/[\d\.]{7,15}/", $realip, $onlineip);
		$realip = !empty($onlineip[0]) ? $onlineip[0] : '0.0.0.0';
	
		return $realip;
	
	}
	static function curl_https_get($url,$data=array()){
		$curl = curl_init(); // 启动一个CURL会话
		curl_setopt($curl, CURLOPT_URL, $url); // 要访问的地址
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0); // 对认证证书来源的检查
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 1); // 从证书中检查SSL加密算法是否存在
		curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']); // 模拟用户使用的浏览器
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1); // 使用自动跳转
		curl_setopt($curl, CURLOPT_AUTOREFERER, 1); // 自动设置Referer
		curl_setopt($curl, CURLOPT_POST, 1); // 发送一个常规的Post请求
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data); // Post提交的数据包
		curl_setopt($curl, CURLOPT_TIMEOUT, 30); // 设置超时限制防止死循环
		curl_setopt($curl, CURLOPT_HEADER, 0); // 显示返回的Header区域内容
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); // 获取的信息以文件流的形式返回
		$tmpInfo = curl_exec($curl); // 执行操作
		if (curl_errno($curl)) {
			echo 'Errno'.curl_error($curl);//捕抓异常
		}
		curl_close($curl); // 关闭CURL会话
		return $tmpInfo; // 返回数据
	}
	/**
	 *	CURL提交 数据
	 * @param unknown_type $url			远程地址
	 * @param unknown_type $post_data	提交 的POST数据
	 * @return unknown					状态码
	 */
	static function curl_post($url,$post_data=array()){
		$url=api_url.$url;
		$post_data["vkey"]=md5(vkey);
		//var_dump($post_data);
		//echo $url;
		//exit(0);
		/*$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
			$output = curl_exec($ch);
			curl_close($ch);
			var_dump($output);
			return $output;*/
		$data=self::curl_multi_fetch(array($url),$post_data);
		return $data;
	}
	//多线程的curl
	static function curl_multi_fetch($urlarr=array(),$post_data){
		$result=$res=$ch=array();
		$nch = 0;
		$mh = curl_multi_init();
		foreach ($urlarr as $nk => $url) {
			$timeout=2;
			$ch[$nch] = curl_init();
			curl_setopt_array($ch[$nch], array(
					CURLOPT_URL => $url,
					CURLOPT_HEADER => false,
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_TIMEOUT => $timeout,
					CURLOPT_POSTFIELDS=>$post_data,
			));
			curl_multi_add_handle($mh, $ch[$nch]);
			++$nch;
		}
		/* wait for performing request */
		do {
			$mrc = curl_multi_exec($mh, $running);
		} while (CURLM_CALL_MULTI_PERFORM == $mrc);
		while ($running && $mrc == CURLM_OK) {
			// wait for network
			if (curl_multi_select($mh, 0.5) > -1) {
				// pull in new data;
				do {
					$mrc = curl_multi_exec($mh, $running);
				} while (CURLM_CALL_MULTI_PERFORM == $mrc);
			}
		}
		if ($mrc != CURLM_OK) {
			error_log("CURL Data Error");
		}
		/* get data */
		$nch = 0;
		foreach ($urlarr as $moudle=>$node) {
			if (($err = curl_error($ch[$nch])) == '') {
				$res[$nch]=curl_multi_getcontent($ch[$nch]);
				$result[$moudle]=$res[$nch];
			}else{
				error_log("curl error");
			}
			curl_multi_remove_handle($mh,$ch[$nch]);
			curl_close($ch[$nch]);
			++$nch;
		}
		curl_multi_close($mh);
		if (count($urlarr)>1) {
			return  $result;
		}
		return  $result[0];
	}
	
	/**
	 *	CURL提交 数据
	 * @param unknown_type $url			远程地址
	 */
	static function curl_get($url,$post_data=array()){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
		$output = curl_exec($ch);
		curl_close($ch);
		return $output;
	}
	//获取网站根目录
	static function get_url_root($url){
		$url=$url?$url:$_SERVER['HTTP_HOST'];
        if(preg_match('%^[\d\.]$%',$url)) return;
        if(preg_match('%[^:\.\/]+(?:(?<ext>\.(?:com|net|org|edu|gov|biz|tv|me|pro|name|cc|co|info|cm))|(?<ctr>\.(?:cn|us|hk|tw|uk|it|fr|br|in|de))|\k<ext>\k<ctr>)+$%i',$url,$match)){
             return $match[0];
        }
        return;
	}
	
}
?>