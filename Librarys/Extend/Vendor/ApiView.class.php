<?php
// +----------------------------------------------------------------------
// | CleverPHP [ WE CAN DO IT JUST BeautySoft ]
// +----------------------------------------------------------------------
// | Copyright (c) 2012~2017 http://git.oschina.net/ceiba/CleverPHP All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: ceiba <ceiba_@126.com>
// +----------------------------------------------------------------------

class ApiView extends View{
	function __construct(){
		parent::__construct();
		$this->is_save_temp_vals=true;
	}
	public  function display($filename, $cache_id = '', $mkid = ''){
	
		$GLOBALS["smarty"]->template_dir=$_SERVER['DOCUMENT_ROOT']."/".appname."/res/WebApp";
	     parent::fetch($filename,false,gmtime());

	     $tmps=array();
	    if ($this->_temp_vals){
	    	foreach ($this->_temp_vals as $k=>$v){
	    		$s='$tmps[] = '.$v."; ";
	    		eval($s);
	    	}
	    	
	    }
	    $tempRes=array();
	    if ($tmps){
	    	$i=1;
	    	foreach ($tmps as $K=>$v){
	    		if (is_array($v)==false){
	    			$tempRes["var_".$i]=$v;
	    			$i++;
	    		}
	    		
	    	}
	    }
	    
	    unset($GLOBALS["smarty"]->_var['assign']);
	    unset($GLOBALS["smarty"]->_var['message']);
	    $vars=$GLOBALS["smarty"]->_var;
	    unset($GLOBALS["smarty"]->_var);
	    if ($tempRes){
	    	$vars=array_merge($vars,$tempRes);
	    }
	    $include_vars=array();
	    if ($this->_include_files){
	    	//是否有包含文件
	    	$i=1;
	    	foreach ($this->_include_files as $k => $file){
	    		if ($file){
	    			parent::fetch($file,false,gmtime().rand(100,985222));
	    			$include_vars["lib_".$i]=$GLOBALS["smarty"]->_var;
	    			unset($include_vars["lib_".$i]["vo"]);
	    			unset($GLOBALS["smarty"]->_var);
	    			
	    		}
	    		$i++;
	    	}
	    }
	    
	    if ($include_vars){
	    	$vars=array_merge($vars,$include_vars);
	    }
	    unset($vars["vo"]);
	   	
	   	var_dump($vars);
	   	//self::JSON($vars,"resault",count($vars));
		
	}
	public  function fetch($filename, $cache_id = '',$mkid=''){
		$this->display($filename, $cache_id = '',$mkid='');
	}  
	static  function JSON($data,$info='',$status=1,$url="",$type='JSON'){
		if (!empty($_GET["data_type"])){
			$type=$_GET["data_type"];
		}
		$result  =  array();
		$result['status']  =  $status;
		$result['info'] =  $info;
		$result['data'] = $data;
		if (!empty($url)){
			$result['url'] = $url;
		}
		if (empty($_GET["callback"])){
	
			if(empty($type)) $type  =   "JSON";
			if(strtoupper($type)=='JSON') {
				header("Content-Type:text/html; charset=utf-8");
				exit(json_encode($result));
			}
		}else {
			$json=$_GET["callback"]."(".json_encode($result).")";
			header("Content-Type:text/html; charset=utf-8");
			exit($json);
		}
	}
	
	
}
?>