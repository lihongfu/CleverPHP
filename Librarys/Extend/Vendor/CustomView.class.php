<?php
namespace CleverPHP\View;
// +----------------------------------------------------------------------
// | CleverPHP [ WE CAN DO IT JUST BeautySoft ]
// +----------------------------------------------------------------------
// | Copyright (c) 2012~2017 http://git.oschina.net/ceiba/CleverPHP All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: ceiba <ceiba_@126.com>
// +----------------------------------------------------------------------

class CustomView{

	protected $partsId=0;
	/**
	 * 普通HTML转部件HTML
	 * @param unknown $html
	 * <div data-block="parts" id="32145">

		OK

		</div>
	 *
	 * @param string $pasr_tpl		部件循环区的tpl
	 */
	protected $data_id=null;
	protected $html=null;
	protected $c_count=0;
	public  $show_type=0;

	public function show($html,$pasr_tpl=""){
		$this->html=$html;
		$rs=array();
		$dom=str_get_html($this->html);
		$res=$dom->find("section");
		foreach ($res as $k=>$v){
			if ($v->hasAttribute('data-block')){
				if($v->getAttribute('id')){
					$this->data_id[]=$v->getAttribute('id');
				}

			}
		}
		return $this->parstFun();

	}
	protected function parstFun(){
		ini_set('display_errors','Off');
		$dom=str_get_html($this->html);
		if ($this->data_id){
			foreach ($this->data_id as $id){
				$res_id1=0;
				$res_id2=0;
				$res_id3=0;
				$res_id4=0;
				$res_id5=0;
				$res=$dom->find("#".$id." div");
				for($i=0;$i<count($res);$i++){
					if ($res[$i]->hasAttribute('data-type')){
						if (empty($res_id1)){
							$res_id1=1;
						}
						$data_type_val=$res[$i]->getAttribute("data-type");

						$html=parts_datas($data_type_val,$id);

						$res[$i]->innertext=$html;
					}
				}
				if ($res_id1==0){
					$res=null;
					$res=$dom->find("#".$id." div div");

					for($i=0;$i<count($res);$i++){
						if ($res[$i]->hasAttribute('data-type')){
							if (empty($res_id2)){
								$res_id2=2;
							}

							$data_type_val=$res[$i]->getAttribute("data-type");

							$html=parts_datas($data_type_val,$id);
							//var_dump($html);
							$res[$i]->innertext=$html;
						}
					}


				}elseif ($res_id1==0 && $res_id2==0){
					$res=null;
					$res=$dom->find("#".$id." div div div");

					for($i=0;$i<count($res);$i++){
						if ($res[$i]->hasAttribute('data-type')){
							if (empty($res_id3)){
								$res_id3=3;
							}
							$data_type_val=$res[$i]->getAttribute("data-type");
							$html=parts_datas($data_type_val,$id);
							$res[$i]->innertext=$html;
						}
					}


				}elseif ($res_id1==0 && $res_id2==0 && $res_id3==0){
					$res=null;
					$res=$dom->find("#".$id." div div div div");

					for($i=0;$i<count($res);$i++){
						if ($res[$i]->hasAttribute('data-type')){
							if (empty($res_id4)){
								$res_id4=4;
							}

							$data_type_val=$res[$i]->getAttribute("data-type");
							$html=parts_datas($data_type_val,$id);
							$res[$i]->innertext=$html;
						}
					}

				}elseif ($res_id1==0 && $res_id2==0 && $res_id3==0 && $res_id4==0){
					$res=null;
					$res=$dom->find("#".$id." div div div div div");

					for($i=0;$i<count($res);$i++){
						if ($res[$i]->hasAttribute('data-type')){
							if (empty($res_id5)){
								$res_id5=5;
							}
							$data_type_val=$res[$i]->getAttribute("data-type");
							$html=parts_datas($data_type_val,$id);
							$res[$i]->innertext=$html;
						}
					}


				}

			}
		}
		$html = $this->infoHtml($dom->save());
		$dom->clear();
		return $html;


	}

	protected function catl_fun(){
		echo $this->parts[0];
	}
	//只保留body标签里的内容
	public function formatBodyAtHtml($html){
		$res=null;
		preg_match('/<body.*?>(.*?)<\/body>/is', $html, $res);
		return $res[1];

	}
	public function formatHeadAtHtml($html){
		$res=null;
		preg_match('/<head.*?>(.*?)<\/head>/is', $html, $res);
		return $res[1];

	}
	protected function showHtm($html){
		return $html;
	}
	protected function explainTag($tpl,$html){
		return $html;
	}
	protected function infoHtml($html){
		if ($this->show_type){
			return $html;
		}
		echo $html;
	}

}


?>