<?php
namespace CleverPHP\Extend\Weixin;
class WeixinObject{
	public $toId;
	public $formId;
	protected $smarty=null;
	function __construct(){
		$this->smarty=new \View();
		
		$this->smarty->template_dir=dirname(__FILE__)."/./xml_template";
		
	}
	//回复文本
	public   function sendMessage($message){
		$this->smarty->assign("fromUser",$this->toId);
		$this->smarty->assign("toUser",$this->formId);
		$this->smarty->assign("times",gmtime());
		$this->smarty->assign("message",$message);
		$xml=$this->smarty->fetch("text_msg.tpl");
		return  $xml;
	}
	
	//回复图文
	public function sendPicMessage($list,$record_count){
		$this->smarty->assign("CreateTime",gmtime());
		$this->smarty->assign("toUser",$this->formId);
		$this->smarty->assign("fromUser",$this->toId );
		if ($record_count>=10) {
			$record_count=10;
		}
		$this->smarty->assign("ArticleCount",$record_count);
		$this->smarty->assign("list",$list);
		$xml=$this->smarty->fetch("pic_article.tpl");
		return $xml;
	}
	
	//回复图片
	public  function sendPicture($media_id){
		$this->smarty->assign("CreateTime",gmtime());
		$this->smarty->assign("toUser",$this->formId);
		$this->smarty->assign("fromUser",$this->toId );
		$this->smarty->assign("mid",$media_id);
		$xml=$this->smarty->fetch("image.tpl");
		return  $xml;
	}
	//回复音频
	public   function sendVoice($media_id){
		$this->smarty->assign("CreateTime",gmtime());
		$this->smarty->assign("toUser",$this->formId);
		$this->smarty->assign("fromUser",$this->toId );
		$this->smarty->assign("mid",$media_id);
		$xml=$this->smarty->fetch("voice.tpl");
		return  $xml;
	}
	
	/**
	 * 回复视频
	 * @param unknown $media_id 微信视频素材id
	 * @param unknown $title	视频标题
	 * @param unknown $description	视频简介 
	 */
	public   function sendVideo($media_id,$title="",$description=""){
		$this->smarty->assign("CreateTime",gmtime());
		$this->smarty->assign("toUser",$this->formId);
		$this->smarty->assign("fromUser",$this->toId);
		$this->smarty->assign("mid",$media_id);
		$this->smarty->assign("title",$title);
		$this->smarty->assign("description",$description);
		$xml=$this->smarty->fetch("video.tpl");
		return  $xml;
	}
	/**
	 * 回复音乐
	 * @param unknown $media_id		缩略图的媒体id
	 * @param string $title			音乐标题
	 * @param string $description	音乐描述
	 * @param string $music_url		音乐链接
	 * @param string $hq_musci_url	高质量音乐链接
	 */
	public  function sendMusic($media_id,$title="",$description="",$music_url="",$hq_musci_url=""){
		$this->smarty->assign("CreateTime",gmtime());
		$this->smarty->assign("toUser",$this->formId);
		$this->smarty->assign("fromUser",$this->toId);
		$this->smarty->assign("mid",$media_id);
		$this->smarty->assign("title",$title);
		$this->smarty->assign("description",$description);
		$this->smarty->assign("music_url",$music_url);
		$this->smarty->assign("hq_musci_url",$hq_musci_url);
		$xml=$this->smarty->fetch("music.tpl");
		return  $xml;
	}
	
	
	/**
	 * 获得Token
	 * @param unknown $AppId
	 * @param unknown $AppSecret
	 * @return unknown[]
	 */
	static public function getAccessTokens($AppId,$AppSecret){
		$url="https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=".$AppId."&secret=".$AppSecret;
		$res=\CoreUtil::curl_https_get($url);
		$data=json_decode($res,true);
		$access_token=$data["access_token"];
		$expires_in=$data["expires_in"];
		return array("access_token"=>$access_token,"expires_in"=>$expires_in);
	}
	/**
	 * 获取微信用户信息
	 * @param unknown $AppId
	 * @param unknown $AppSecret
	 * @param string $file
	 * @return array || string
	 */
	static public  function getWeixinOpenUserData($openId,$AppId,$AppSecret,$file=""){
		if (self::getAccessToken($AppId,$AppSecret)) {
			$url="https://api.weixin.qq.com/cgi-bin/user/info?access_token=".self::getAccessToken($AppId, $AppSecret)."&openid=".$openId."&lang=zh_CN";
			$res=\CoreUtil::curl_https_get($url);
			$data=json_decode($res,true);
			if (empty($file)) {
				return $data;
			}
			return $data[$file];
		}else{
			return false;
		}
	}
	/**
	 * 主动发送微信消息
	 * @param unknown $AppId
	 * @param unknown $AppSecret
	 * @param unknown $toOpenId
	 * @param unknown $message
	 * @return null
	 */
	static public  function sendWeixinMeesage($AppId,$AppSecret,$toOpenId,$message){
		$cache_name="wx_".md5($toOpenId.urlencode($message));
		
		$cache=new \Cache();
		if ($cache->get($cache_name)){
			return true;
		}
	
		$AccessToken=self::getAccessToken($AppId,$AppSecret);
	
		$json='{
    		"touser": "'.$toOpenId.'",
    		"msgtype": "text",
    		"text": {
       			 "content": "'.$message.'"
         }
		}';
		$url="https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token={$AccessToken}";
		$res=Util::curl_https_get($url,$json);
		$cache->set($cache_name, 1);
	}
	
	/**
	 * 生成二维码
	 * @param unknown $AppId
	 * @param unknown $AppSecret
	 * @param number $type	0=>QR_SCENE,1=>QR_LIMIT_SCENE
	 */
	static public function  createQRCode($AppId,$AppSecret,$sceneId="",$type=0){
		$scene_id=rand(1001, PHP_INT_MAX);
		$q_str="QR_SCENE";
		if ($type) {
			$scene_id=rand(10, 10000);
			$q_str="QR_LIMIT_SCENE";
		}
		if (!empty($sceneId)){
			$scene_id=$sceneId;
		}
	
		$data='{"action_name": "'.$q_str.'", "action_info": {"scene": {"scene_id": '.$scene_id.'}}}';
		$url="https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=".getAccessToken($AppId,$AppSecret);
		$res=\CoreUtil::curl_https_get($url,$data);
		$res=json_decode($res,true);
		if (!empty($res["ticket"])) {
			$url="https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=".$res["ticket"];
			$GLOBALS["no_del_cache"]=true;
			$_SESSION["ticket"]=md5($res["ticket"]);
			$caches=array("scene_id"=>$scene_id,"add_time"=>gmtime());
			$cache=new \Cache();
			$cache->set(md5($res["ticket"]), $caches);
			echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><img src="'.$url.'" />';
		}else{
			echo " ticket is null ";
		}
	
	}
	
	
	
}
?>