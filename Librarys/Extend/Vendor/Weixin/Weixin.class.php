<?php
namespace CleverPHP\Extend\Weixin;
include_once "WeixinObject.class.php";
class Weixin{
	public $errors=null;
	private $token=null;
	private $object;
	public $projectName;
	public $appName;
	private $dir;
	public $calssName;
	private $mesgType;
	private $fromUsername;
	private $toUsername;
	private $keyword;
	
	//是否调式模式
	public $is_debug=false;
	public $debug_event="subscribe";
	public $debug_is_event=false;
	public $is_log=false;
	function __construct($token=null){
		if(!empty($token)){
			$this->token=$token;
		}
		$trace=debug_backtrace();
		$file=$trace[0]["file"];
		if (!is_file($file)){
			exit("php version < 7.0");
		}
		$class=@$trace[3]["object"];
		$this->calssName=$class;
		$info=pathinfo($file);
		$info=pathinfo($info["dirname"]);
		$info=pathinfo($info["dirname"]);
		$projectName=$info["basename"]??"Master";
		$this->projectName=$projectName;
		$info=pathinfo($info["dirname"]);
		$this->appName=$info["basename"];
		
	}

	protected  function responseMsg()
	{
		//get post data, May be due to the different environments
		$postStr = file_get_contents("php://input");
		$this->logs("接收到数据:".$postStr);
		//extract post data
		if (!empty($postStr) || $this->is_debug){
			$this->logs("解包到数据:".$postStr);
			$postObj = simplexml_load_string($postStr, 'SimpleXMLElement', LIBXML_NOCDATA);
			
			if ($this->is_debug && empty($postObj)){
				$postObj=$this->createDebugPostStr();
			}
			
			if (!$this->is_debug){
				//开发者微信号
				$this->fromUsername = $postObj->FromUserName;	
				//目标者微信号
				$this->toUsername = $postObj->ToUserName;
			}
			
			
			//接收到用户上传的内容
			$this->keyword = trim($postObj->Content);
			
			//时间
			$time = time();
			//类型
			$msgType=trim($postObj->MsgType);
			$this->mesgType=$msgType;
			$textTpl = "";
			if ($msgType=="voice") {
				//语音
				$this->keyword=$postObj->Recognition;
			}
			if(!empty( $this->keyword ))
			{
				//信息
				$this->logs("用户回复内容为:".$this->keyword);
				if ($msgType == "text"){
					$xml=$this->call("text", array("keyword"=>$this->keyword));
				}else{
					if ($this->is_debug){
						echo " Invalid service";
						exit();
					}
					
				}
			}else{
				if ($msgType=="event"){
					$Event=$postObj->Event;
					$this->logs("接收到事件为:".$Event);
					//事件
					if (strtolower($Event)=="subscribe") {
						//关注
						
						$data=array(
							"event"=>$postObj->Event,
							"fromUsername"=>$this->fromUsername,
							"toUsername"=>$this->toUsername,
						);
						if(isset($postObj->Ticket)){
							$data["ticket"]=$postObj->Ticket;
						}
						
						$this->logs("用户关注:".$postStr.$data);
						$this->call("subscribe", $data);
					}elseif (strtolower($Event)=="scan") {
						
						//二维码扫码(除用户关注外)
						$data=array(
							"event"=>$postObj->Event,
							"fromUsername"=>$this->fromUsername,
							"toUsername"=>$this->toUsername,
						);
						if(isset($postObj->Ticket)){
							$data["ticket"]=$postObj->Ticket;
						}
						
						$this->logs("扫描二维码:".$postStr.$data);
						$this->call("scan", $data);
						
					
					}elseif (strtolower($Event)=="unsubscribe") {
						//取消关注
						
						$data=array(
							"event"=>$postObj->Event,
							"fromUsername"=>$this->fromUsername,
							"toUsername"=>$this->toUsername,
						);
						
						$this->logs("用户取消关注:".$postStr);
						$this->call("unsubscribe", $data);
					}elseif (strtolower($Event)=="click") {
						//菜单点击 
						$this->logs("菜单点击:".$postStr);
						$this->call("click", array("event"=>$postObj->Event,"EventKey"=>$postObj->EventKey));
					}
					
				}
			}
	
		}else {
			$this->logs("接收不到:".$postStr);
			echo "";
			exit;
		}
	}
	
	public function call($method,$args) {
		if (is_array($args)==false){
			$args=array($args);
		}
		
		$args["weixin"]="1";
		$datas=$args;
		if ($this->formatGetAges()){
			$datas=array_merge($datas,$this->formatGetAges());
		}
		$data=array(
				"method"=>$method."Callback",
				"datas"=>$datas
		);
		
		$this->callFunction($data);
	}
	
	protected  function  callFunction(array $data){
		if(!$this->calssName) exit("class is null");
		if (isset($data["method"])){
			$functionName=$data["method"];
			unset($data["method"]);
			$obj=new WeixinObject();
			$obj->toId=$this->toUsername;
			$obj->formId=$this->fromUsername;
			return $this->calssName->$functionName($obj,$data["datas"]);
		}
	}
	
	//格式化GET参数，用于分页
	protected function formatGetAges(){
		$page_args=$_REQUEST;
		if (!empty($_POST)){
			$page_args=@array_merge($page_args,$_POST);
		}
		if (!empty($_GET)){
			$page_args=@array_merge($page_args,$_GET);
		}
		unset($page_args["a"]);
		unset($page_args["m"]);
		unset($page_args["q"]);
		unset($page_args["app"]);
		unset($page_args["_str"]);
		unset($page_args["_"]);
		unset($page_args["url"]);
		unset($page_args["page"]);
		unset($page_args["act"]);
		return $page_args;
	}
	
	
	//验证
	public  function valid($token)
	{
		$this->token=$token;
		$this->logs("开始进入验证流程".$token);
		if (isset($_GET["echostr"])){
			$echoStr = $_GET["echostr"];
			//valid signature , option
			if($this->checkSignature()){
				$this->logs("验证通过");
				echo $echoStr;
				exit();
			}else{
				$this->logs("验证失败");
				exit("token ".$token." is error");
			}
		}else{
			$this->logs("处理信息");
			$this->responseMsg();
			
		}
	}
	
	private  function checkSignature()
	{
		$token=$this->token;
		if (isset($_GET["signature"]) && isset($_GET["timestamp"]) && isset($_GET["nonce"])){
			$this->logs("所有验证参数通过");
			$signature = $_GET["signature"];
			$timestamp = $_GET["timestamp"];
			$nonce = $_GET["nonce"];
			
			$token = $token;
			$tmpArr = array($token, $timestamp, $nonce);
			sort($tmpArr);
			$tmpStr = implode( $tmpArr );
			$tmpStr = sha1( $tmpStr );
			if( $tmpStr == $signature ){
				return true;
			
			}else{
				$this->logs("非法的验证参数");
				return false;
			}
		}
		
	}
	private function array2object($array) {
		
		if (is_array($array)) {
			$obj = new \Objects();
			
			foreach ($array as $key => $val){
				$obj->$key = $val;
			}
		}else {
			$obj = $array;
		}
		return $obj;
	}
	
	protected function createDebugPostStr(){
		
		$postStr=array(
				"Content"=>"test ing...".time(),
				"MsgType"=>"text",
	
		);
		if ($this->debug_is_event){
			$postStr["Content"]=null;
			$postStr["MsgType"]="event";
			$postStr["Event"]=$this->debug_event;
		}
		$this->toUsername="toUsername_".time();
		$this->fromUsername="fromUsername_".time();
		return $this->array2object($postStr);
	}

	protected function logs($logs){
		if (!$this->is_log) return ;
		$logs.=" client ip ".\CoreUtil::getIP();
		if ($this->projectName){
			return  CleverPHPLogs($logs,$this->appName,array(),"weixin");
		}
	}
	static protected function slogs($logs){
		$weixin=new Weixin(null);
		return $weixin->logs($logs);
	}
	
}

?>