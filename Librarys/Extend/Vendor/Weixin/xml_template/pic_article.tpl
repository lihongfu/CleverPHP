<xml>
<ToUserName><![CDATA[{$toUser}]]></ToUserName>
<FromUserName><![CDATA[{$fromUser}]]></FromUserName>
<CreateTime>{$CreateTime}</CreateTime>
<MsgType><![CDATA[news]]></MsgType>
<ArticleCount>{$ArticleCount}</ArticleCount>
<Articles>
	<!--{foreach from=$list item=vo}-->
	<item>
		<Title><![CDATA[{$vo.Title}]]></Title> 
		<Description><![CDATA[{$vo.Description}]]></Description>
		<PicUrl><![CDATA[{$vo.PicUrl}]]></PicUrl>
		<Url><![CDATA[{$vo.Url}]]></Url>
	</item>
    <!--{/foreach}-->
</Articles>
</xml>