<?php
// +----------------------------------------------------------------------
// | CleverPHP [ WE CAN DO IT JUST BeautySoft ]
// +----------------------------------------------------------------------
// | Copyright (c) 2012~2017 http://git.oschina.net/ceiba/CleverPHP All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: ceiba <ceiba_@126.com>
// +----------------------------------------------------------------------

class Pager{
	/**
	 * 设置分页
	 * @param unknown $url				url
	 * @param unknown $param			参数
	 * @param unknown $record_count		记录条数
	 * @param number $page				当页
	 * @param number $size				分页大小
	 */
	static public   function set_pager($url, $param, $record_count, $page = 1, $size = 10)
	{
		if (isset($_REQUEST["act"])){
			$param["act"]=$_REQUEST["act"];
		}
		if (isset($_REQUEST["page_size"])){
			$param["page_size"]=$_REQUEST["page_size"];
		}
		if (isset($_REQUEST["size"])){
			$param["page_size"]=$_REQUEST["size"];
		}
		$size = intval($size);
		if ($size < 1)
		{
			$size = 10;
		}
	
		$page = intval($page);
		if ($page < 1)
		{
			$page = 1;
		}
		$record_count = intval($record_count);
		$page_count = $record_count > 0 ? intval(ceil($record_count / $size)) : 1;
		if ($page > $page_count)
		{
			$page = $page_count;
		}
		/* 分页样式 */
		$pager['styleid'] = isset($GLOBALS['_CFG']['page_style'])? intval($GLOBALS['_CFG']['page_style']) : 0;
	
		$page_prev  = ($page > 1) ? $page - 1 : 1;
		$page_next  = ($page < $page_count) ? $page + 1 : $page_count;
	
		/* 将参数合成url字串 */
		$param_url = '';
		foreach ($param AS $key => $value)
		{
			if (!empty($value)){
				$param_url .=  "-".$value ;
	
			}
		}
		$param_url=ltrim($param_url,"-");
		$pager['url']          = $url;
		$pager['start']        = ($page -1) * $size;
		$pager['page']         = $page;
		$pager['size']         = $size;
		$pager['record_count'] = $record_count;
		$pager['page_count']   = $page_count;
	
		if ($pager['styleid'] == 0)
		{
			$pager['page_first']   = $url . $param_url . 'page=1';
			$pager['page_prev']    = $url . $param_url . 'page=' . $page_prev;
			$pager['page_next']    = $url . $param_url . 'page=' . $page_next;
			$pager['page_last']    = $url . $param_url . 'page=' . $page_count;
			$pager['array']        = array();
			for ($i = 1; $i <= $page_count; $i++)
			{
			$pager['array'][$i] = $i;
			}
			}
			else
			{
			$_pagenum = 10;     // 显示的页码
			$_offset = 2;       // 当前页偏移值
			$_from = $_to = 0;  // 开始页, 结束页
			if($_pagenum > $page_count)
			{
			$_from = 1;
				$_to = $page_count;
			}
			else
			{
			$_from = $page - $_offset;
				$_to = $_from + $_pagenum - 1;
				if($_from < 1)
				{
				$_to = $page + 1 - $_from;
				$_from = 1;
				if($_to - $_from < $_pagenum)
				{
				$_to = $_pagenum;
				}
				}
				elseif($_to > $page_count)
				{
				$_from = $page_count - $_pagenum + 1;
				$_to = $page_count;
				}
				}
				$url_format = "";
				$pager['page_first'] = ($page - $_offset > 1 && $_pagenum < $page_count) ? $url_format . 1 : '';
				$pager['page_prev']  = ($page > 1) ? $url_format . $page_prev : '';
				$pager['page_next']  = ($page < $page_count) ? $page_next : '';
				$pager['page_last']  = ($_to < $page_count) ? $page_count : '';
				$pager['page_kbd']  = ($_pagenum < $page_count) ? true : false;
						$pager['page_number'] = array();
						for ($i=$_from;$i<=$_to;++$i)
						{
						$pager['page_number'][$i] =  $i;
			}
			}
			$pager['param_url'] = "";
			if ($param){
				$url="";
					foreach ($param as $key=>$value){
						$url.="&".$key."=".$value;
					}
					$pager["param_url"]="?".$url;
					$pager["param_url"]=str_replace("?&", "?", $pager["param_url"]);
			}
			return $pager;
	 }
	 //显示分页
	 public function show(){
	 	
	 	
	 }
}
?>