<?php
// +----------------------------------------------------------------------
// | CleverPHP [ WE CAN DO IT JUST BeautySoft ]
// +----------------------------------------------------------------------
// | Copyright (c) 2012~2017 http://git.oschina.net/ceiba/CleverPHP All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: ceiba <ceiba_@126.com>
// +----------------------------------------------------------------------

//+---------
//|缓存类，中间件
//+---------
class Cache{
	protected $cacheName;
	public $cacheObj;
	protected $Drive;
	function __construct($Drive=""){
		if (!$this->cacheObj){
			$this->Drive=$Drive;
			$this->_init();
		}
		
	}
	
	protected function _init(){
			$Drive=$this->Drive;
			$config=C();
			if (isset($config["cache_drive"]) && !empty($config["cache_drive"])){
				$Drive=$config["cache_drive"];
			}
			if (empty($Drive)){
				$this->cacheName="FileCache";
			}else {
				$this->cacheName="Cache".ucfirst($Drive);
			}
				
			$fiel=dirname(__FILE__).'/../Librarys/Extend/Drive/'.$this->cacheName.".class.php";
				
			if(is_file($fiel)){
				require_once "{$fiel}";
				$class=$this->cacheName;
				$this->cacheObj=new $class();
				
			}else{
				throw  new Exception("{$fiel}"."文件不存在");
			}
	}
	
	/**
	 * 设置缓存
	 * @param string $key
	 * @param string $data
	 * @param int $expire
	 */
	public function set($key,$data,$expire=""){
		return @$this->cacheObj->setCache($key,$data,$expire);
	}
	/**
	 * 获取缓存
	 * @param string $key
	 */
	public function get($key){
		
		return $this->cacheObj->getCache($key);
	}
	/**
	 * 删除缓存
	 * @param string $key
	 */
	public function del($key){
		return $this->cacheObj->delCache($key);
	}
	
	
	
	
}
?>