<?php
// +----------------------------------------------------------------------
// | CleverPHP [ WE CAN DO IT JUST BeautySoft ]
// +----------------------------------------------------------------------
// | Copyright (c) 2012~2017 http://git.oschina.net/ceiba/CleverPHP All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: ceiba <ceiba_@126.com>
// +----------------------------------------------------------------------



//+------------------
//|PDO连接Mysql
//+------------------
class PDO_DB_Conn{
   static  public  $PDOs;
    
    /**
     * 返回数据连接对像
     *
     * @return PDO_Mysql
     */
	function __construct(){
  	   try {
  	   	$db_host=C("db_host");
  	   	
  	   	$db_name=C("db_name");
  	   	
  	   	self::$PDOs=new PDO("mysql:host=$db_host;dbname=$db_name",C("db_user"),C("db_pwd"));  
  	   	
  	   	self::$PDOs->query("set names utf8");
  	   	  
  	    self::$PDOs->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);	
  	   }catch (PDOException $e){ 
  	   	 	   	    
  	   	   print $e->getMessage();
  	   	   exit();
  	   }  	
    }
}

?>