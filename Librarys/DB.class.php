<?php
namespace MYSQLDB;
// +----------------------------------------------------------------------
// | CleverPHP [ WE CAN DO IT JUST BeautySoft ]
// +----------------------------------------------------------------------
// | Copyright (c) 2012~2017 http://git.oschina.net/ceiba/CleverPHP All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: ceiba <ceiba_@126.com>
// +----------------------------------------------------------------------

Vendor("CoreUtil.class.php");
class DB{
	protected static  $insertLastId;
	protected   $obj;
	function __construct(){
		if(!$this->obj){
			$this->obj=MOD("");
			
		}
		
	}
	public function getAll($sql) {
		$this->obj->sql=$sql;
		$list=$this->obj->read();
		return  $list;
	}
	public function getRow($sql){
		$this->obj->sql=$sql;
		$find=$this->obj->find();
		return  $find;
	}
	public function getOne($sql){
		$this->obj->sql=$sql;
		$find=$this->obj->find();
		if($find){
			$resautl=array_values($find);
			return  $resautl[0];
		}
		
	}
	public function fetchRow($query){
		return $this->obj->fetchRow($query);
		
	}
	public function query($sql) {
		$this->obj->sql=$sql;
		return $this->obj->query();
	}
    
	public function autoExecute($table,$field_values,$mode = 'INSERT',$where = '',$querymode = '')
	{
		$field_values=$this->formatFieldValues($table, $field_values);
		$this->obj->TableName=$table;
		if ($mode=="INSERT"){
			 $res=$this->obj->data($field_values)->create();
			 self::$insertLastId=$this->obj->getlastInsertId();
			 return $res;
		}else{
			if (!empty($where)){
				return $this->obj->data($field_values)->where($where)->update();
			}
			
		}
		return false;
		
	}
	public function insert_id() {
		
		return self::$insertLastId;
		
	}
	public function selectLimit($sql, $num, $start = 0){
		if ($start == 0)
        {
            $sql .= ' LIMIT ' . intval($num);
        }
        else
        {
            $sql .= ' LIMIT ' . $start . ', ' . intval($num);
        }

        return $this->query($sql);
	}
	
	
	public function getCol($sql) {
		$res = $this->query($sql);
		if ($res !== false)
        {
            $arr = array();
            while ($row = $this->fetchRow($res))
            {
                $arr[] = $row[0];
            }

            return $arr;
        }
        else
        {
            return false;
        }
	}
	public function getColCached($sql) {
		$res = $this->query($sql);
		if ($res !== false)
        {
            $arr = array();
            while ($row = $this->fetchRow($res))
            {
                $arr[] = $row[0];
            }

            return $arr;
        }
        else
        {
            return false;
        }
	}
	protected  function formatFieldValues($table,$field_values){
		$fields=$this->obj->getColumnMetas($table);
		$fields=\CoreUtil::array_diffs($field_values,$fields);
		if ($fields){
			foreach ($fields as $k=>$v){
				if (array_key_exists($k, $field_values)){
					unset($field_values[$k]);
				}
			}
			return $field_values;
		}
		return $field_values;
	}
}
?>

