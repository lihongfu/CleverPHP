<?php
// +----------------------------------------------------------------------
// | CleverPHP [ WE CAN DO IT JUST BeautySoft ]
// +----------------------------------------------------------------------
// | Copyright (c) 2012~2017 http://git.oschina.net/ceiba/CleverPHP All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: ceiba <ceiba_@126.com>
// +----------------------------------------------------------------------
/**
 * RPC异步服务
 */
namespace CleverPHP\Librarys;
class Rpc {
	public $object;
	public $projectName;
	public $appName;
	public $dir;
	public $calssName;
	function __autoload(){
	}
	public function __call($method,$args) {
		$trace=debug_backtrace();
		$file=$trace[0]["file"];
		$class=$trace[2]["class"];
		$this->calssName=$class;
		$info=pathinfo($file);
	
		$info=pathinfo($info["dirname"]);
		
		$info=pathinfo($info["dirname"]);
		
		$projectName=$info["basename"]??"Master";
		$this->projectName=$projectName;
		
		$info=pathinfo($info["dirname"]);
		$this->appName=$info["basename"];
		$args["rpc"]="1";
		$datas=$args;
		if ($this->formatGetAges()){
			$datas=array_merge($datas,$this->formatGetAges());
		}
		$data=array(
				"method"=>$method,
				"datas"=>$datas
		);
		
		$this->callFunction($projectName.".".$class,$data);
	}
	protected  function  callFunction($class,array $data){
		if (isset($data["method"])){
			Drive("FileCache");
			$functionName=$data["method"];
			$Cache=new \FileCache();
			$Cache->cacheDir="temp/tasks";
			$services=$this->appName.".".$this->projectName.".".$this->calssName.".".$functionName;
			$inser_data=array("services"=>$services,"data"=>json_encode($data),"add_time"=>gmtime());
			return $Cache->setCache(gmtime(),$inser_data);
		}
	}
	function __construct(){
		/* $trace=debug_backtrace();
		$class=$trace[1]["class"];
		$file=$trace[0]["file"];
		$function=$trace[1]["function"]; */
		
		
	}
	//格式化GET参数，用于分页
	protected function formatGetAges(){
		$page_args=$_REQUEST;
		if (!empty($_POST)){
			$page_args=@array_merge($page_args,$_POST);
		}
		if (!empty($_GET)){
			$page_args=@array_merge($page_args,$_GET);
		}
		//unset($page_args["a"]);
		//unset($page_args["m"]);
		unset($page_args["q"]);
		unset($page_args["app"]);
		unset($page_args["_str"]);
		unset($page_args["_"]);
		unset($page_args["url"]);
		unset($page_args["page"]);
		unset($page_args["act"]);
		return $page_args;
	}
	
	
	
	
}
//执行RPC服务 
class RpcAct{
	protected  $servicesObj;
	protected $project;
	protected $appName;
	protected $dir;
	//消息列表
	public function httpsqs(){
		if (isset($_REQUEST["goservice"])){
			$serviceqs=trim($_REQUEST["goservice"]);
			$m=null;
			$a=null;
			$p=null;
			$c=null;
			if (stripos($serviceqs,".")){
				$arr=explode(".",$serviceqs);
				if (count($arr)>=2){
					$p=@$arr[0];
					$m=@$arr[1];
					$c=@$arr[2];
					$a=@$arr[3];
				}else{
					echo "错误的参数 ";
				}
			}else{
				$m=trim($serviceqs,".");
				$a="index";
			}
			$this->project=$p;
			$this->appName=$m;
			$this->_init();
			$c=str_replace("Action", "", $c);
			$GLOBALS["RPC"]=1;
			$this->servicesObj= A($c);
			$this->go($a);
				
		}
	}
	//逻辑执行
	protected function go($methods){
		try {
			if ($this->servicesObj){
				$GLOBALS["RPC"]=1;
				if (method_exists($this->servicesObj,$methods)){
					
					$res= $this->servicesObj->$methods();
					if (@json_decode($res,true)){
						$res=json_decode($res,true);
						$res=$res["status"];
						if (!$res){
							$res=$res["info"];
						}
					}
					$this->writeRpcLogs("执行成功:队列".$methods."执行成功，返回结果：\n".$res);
					
				}else{
					$this->writeRpcLogs("执行失败：".$methods."是无效的服务");
					exit("invalid services");
				}
	
			}else{
				$this->writeRpcLogs("执行失败：执行不到".$methods."服务");
				exit("empty services");
			}
		} catch (Exception $e) {
			$this->writeRpcLogs("程序异常：".$e->getMessage());
			die($e->getMessage());
		}
	}
	
	protected function _init(){
		$GLOBALS["APP_NAME"]=$this->project."/".$this->appName;
		$this->dir=$_SERVER['DOCUMENT_ROOT']."/".$this->project."/".$this->appName;
		require($this->dir.'/Common/function.php');
		require($this->dir.'/Common/insert.php');
		require($this->dir.'/Actions/BaseAction.class.php');
		require($this->dir.'/assign.php');
	}
	protected function writeRpcLogs($logs){
		//$logs,$projectName=null,$extend_logs=array(),$logName="logs"
		 return  CleverPHPLogs($logs,$this->project,array(),"rpc");
		
	}

}
?>
