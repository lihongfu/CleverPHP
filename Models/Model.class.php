<?php
// +----------------------------------------------------------------------
// | CleverPHP [ WE CAN DO IT JUST BeautySoft ]
// +----------------------------------------------------------------------
// | Copyright (c) 2012~2017 http://git.oschina.net/ceiba/CleverPHP All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: ceiba <ceiba_@126.com>
// +----------------------------------------------------------------------

//+--------------
//|实现IModel
//+--------------
class Model {
	public  $sql;
	public  $TableName;
	public $lastSql;
	public $error;
	protected $POD;
	protected $options  =   array();
	public function __call($method,$args) {
		if(in_array(strtolower($method),array('field','where','order','limit',"data"),true)) {
			// 连贯操作的实现
			$this->options[strtolower($method)] =   $args[0];
			return $this;
				
		}else{
			exit("当前模型不存在".$method."方法");
		}
	}
	public function __construct($tableName=""){
		if (empty($tableName)) {
			$this->TableName=C("db_table_prefix").str_replace("Model", "", get_called_class());
		}else{
			$this->TableName=C("db_table_prefix").$tableName;
		}
		new PDO_DB_Conn();
		$this->POD=PDO_DB_Conn::$PDOs;
	}
	public function getlastInsertId(){
		if ($this->POD){
			return $this->POD->lastInsertId();
		}
		
	}
	
	/**
	 * 插入数据
	 * @see IModel::create()
	 */
	public function create(){
		try {
			$data=$this->options["data"];
			$field=null;
			$val=null;
			if ($data){
				foreach ($data as $key=>$value) {
					if (is_string($value)){
						$value=trim(strip_tags($value));
						$value="'$value'";
					}else {
						$value=intval($value);
					}
					$val.=$value.",";
					$field.="`".$key."`,";
				}
				$field=rtrim($field,",");
				$val=rtrim($val,",");
				$this->sql="insert ".$this->TableName." ($field)"." Values($val)";
			}else{
				exit("数据为空");
			}
			
			if ($this->POD) {
				$rs=$this->POD->prepare($this->sql);
				return $rs->execute();
			}else {
				$this->lastSql=$rs->queryString;
				$this->error=$pdo->errorInfo();
				return false;
			}
			
		} catch (PDOException $e) {
			runException($e);
		}		
		
	}
	/**
	 * 更新的数据
	 * @see IModel::update()
	 */
	public function update(){
		try {
			$where=" where ".$this->options["where"];
			$data=$this->options["data"];
			$field=null;
			if ($data){
				foreach ($data as $key=>$value) {
					if (is_string($value)){
						$value="'$value'";
					}else {
						$value=intval($value);
					}
			
					$field.=",`".$key."`=".$value;
				}
				$sets=trim($field,",");
				$sets="set ".$sets;
				$this->sql="Update ".$this->TableName." $sets"." $where ";
			}else{
				exit("数据为空");
			}
			if ($this->POD) {
					
				$rs=$this->POD->prepare($this->sql);
				 
				return $rs->execute();
					
			}else {
				$this->error=$this->POD->errorInfo();
				return false;
			}
			
		} catch (PDOException $e) {
			runException($e);
		}	
		
	}
	/**
	 * 读取数据
	 * @see IModel::read()
	 */
	public function read(){
   	  try {
   	  	if ($pdo=$this->POD) {
   	  		if (array_key_exists("where", $this->options)){
   	  			$where=" where ".$this->options["where"];
   	  		}else{
   	  			$where=null;
   	  		}
   	  		if (array_key_exists("order", $this->options)){
   	  			$order=" order ".$this->options["order"];
   	  		}else{
   	  			$order=null;
   	  		}
   	  		$limit="";
   	  		if (array_key_exists("limit", $this->options)){
   	  			$limit=" limit ".$this->options["limit"];
   	  	
   	  		}
   	  		if (array_key_exists("field", $this->options)){
   	  			$field=$this->options["field"];
   	  		}else{
   	  			$field=" * ";
   	  		}
   	  		if (!$this->sql){
   	  			$this->sql="select ".$field." from ".$this->TableName.$where.$order.$limit;
   	  		}
   	  		$pdo->query("set names utf8");
   	  		$rs=$pdo->prepare($this->sql);
   	  		$rs->execute();
			$rs->setFetchMode(PDO::FETCH_ASSOC);
   	  		$this->lastSql=$rs->queryString;
   	  		return  $rs->fetchAll();
   	  	}else {
   	  		return false;
   	  	}
   	  	
   	  } catch (PDOException $e) {
   	  	 runException($e);
   	  }finally {
   	  	
   	  }
   	  
	}
	/**
	 * 读取单条数据
	 * @see IModel::read()
	 */
	public function find(){
		try {
			
			if ($pdo=$this->POD) {
				if (array_key_exists("where", $this->options)){
					$where=" where ".$this->options["where"];
				}else{
					$where=null;
				}
				if (array_key_exists("order", $this->options)){
					$order=" order ".$this->options["order"];
				}else{
					$order=null;
				}
				$limit=" limit 0,1";
				if (array_key_exists("field", $this->options)){
					$field="  ".$this->options["field"]." ";
				}else{
					$field=" * ";
				}
				if (!$this->sql){
					$this->sql="select ".$field." from ".$this->TableName.$where.$order.$limit;
				}
				$pdo->query("set names utf8");
				$rs=$pdo->prepare($this->sql);
				$rs->execute();
				$rs->setFetchMode(PDO::FETCH_ASSOC);
				$this->lastSql=$rs->queryString;
				return  $rs->fetch();
			}else {
				return false;
			}
			
		} catch (Exception $e) {
			runException($e);
		}
	
	}
	
	public function fetchRow($query){
		if ($pdo=$this->POD) {
			return $query->fetch(PDO::FETCH_ASSOC);
			
		}
		return false;
	}
	/**
	 * 执行Query
	 * @see IModel::read()
	 */
	public function query(){
		try {
			
			if ($pdo=$this->POD) {
				return $pdo->query($this->sql);
			}else {
				return false;
			}
			
			
		} catch (PDOException $e) {
			runException($e);
		}
	
	}

	/**
	 * 统计总启记录
	 * @return number|boolean
	 */
	public function count(){
		try {
			
			if ($pdo=$this->POD) {
				if (array_key_exists("where", $this->options)){
					$where=" where ".$this->options["where"];
				}else{
					$where=null;
				}
				if (!$this->sql){
					$this->sql="select * from ".$this->TableName.$where;
				}
				$pdo->query("set names utf8");
				$rs=$pdo->prepare($this->sql);
				$rs->execute();
				$count=$rs->rowCount();
				$this->lastSql=$rs->queryString;
				return $count;
			}else {
				return false;
			}
			
		} catch (Exception $e) {
			
			runException($e);
		}
	}
	/**
	 * 删除数据
	 * Enter description here ...
	 */
	function delete(){
   	    try {
   	    	if ($pdo=$this->POD) {
   	    		if (array_key_exists("where", $this->options)){
   	    			 
   	    			$where=" WHERE ".$this->options["where"];
   	    	
   	    			$this->sql="DELETE FROM {$this->TableName} ".$where;
   	    		}else{
   	    			$this->error="where条件不能为空";
   	    			return false;
   	    		}
   	    		 
   	    		$rs=$pdo->prepare($this->sql);
   	    		$this->lastSql=$rs->queryString;
   	    		return $rs->execute();
   	    		 
   	    	}else {
   	    		return false;
   	    	}
   	    	
   	    } catch (PDOException $e) {
   	    	runException($e);
   	    }
	}
	public function getColumnMetas($tableName=null){
		try {
			$tableName?$tableName:$this->TableName;
			if (empty($tableName)) return array();
			if ($pdo=$this->POD) {
				$fields=array();
				$pdo->query("set names utf8");
				$stmt = $pdo->query("select * from $tableName");
  				for($i=0; $i<$stmt->columnCount(); $i++) {
    				$fields[$stmt->getColumnMeta($i)['name']]=1;
  				}
  				return $fields;
			}else {
				return array();
			}
				
		} catch (Exception $e) {
				
			runException($e);
		}
	}
	

	
	
	
}
?>