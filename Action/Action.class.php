<?php 
// +----------------------------------------------------------------------
// | CleverPHP [ WE CAN DO IT JUST BeautySoft ]
// +----------------------------------------------------------------------
// | Copyright (c) 2012~2017 http://git.oschina.net/ceiba/CleverPHP All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: ceiba <ceiba_@126.com>
// +----------------------------------------------------------------------

class Action{
	public  $smarty;
	public  $assign=NULL;
	public  $user_id;
	function __construct(){
		$this->_init();
	}
	private function _init(){
		
		$this->user_id=@$_SESSION["user_id"];
		$this->smarty=@$GLOBALS["smarty"];
	}
	public function display($file){
		$this->_init();
		$cache_id = md5($this->smarty->template_dir)."_".urlencode($file);
		if (!$this->smarty->is_cached($file, $cache_id))
		{
			if ($this->assign){
				foreach ($this->assign as $key=>$value){
					$this->smarty->assign($key,$value);
				}
			}
		}
		if (isset($_GET["json"])){
		    $this->infoJson();
		    exit();
		}
// 		if (strtolower(accessType())=="webapp"){
// 			$this->infoapp($file, $cache_id);
// 			exit();
// 		}
	
		$GLOBALS["smarty"]->display($file, $cache_id);
	}
	//当GET请求带有json=1时，输出json格式
	protected function infoJson(){
		$data=json_decode(json_encode($this->assign),true);
		Util::JSON($data,"resautl",count($data)?1:0);
	}
	public function fetch($file){
		$this->_init();
		$cache_id = md5($this->smarty->template_dir)."_".urlencode($file);
		if (!$this->smarty->is_cached($file, $cache_id))
		{
			if ($this->assign){
				foreach ($this->assign as $key=>$value){
					$this->smarty->assign($key,$value);
				}
			}
		}
		if (isset($_GET["webapp"])){
			$this->infoapp($file, $cache_id);
			exit();
		}
		return $this->smarty->fetch($file, $cache_id);
	}
	public function infoapp($file, $cache_id){
		$GLOBALS["smarty"]->template_dir=$_SERVER['DOCUMENT_ROOT']."/".appname."/res/WebApp";
		$tpl=$this->smarty->fetch($file, md5($GLOBALS["smarty"]->template_dir)."_".$file);
		echo $tpl;
	}
	
	
}


?>