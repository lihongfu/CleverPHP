<?php 
// +----------------------------------------------------------------------
// | CleverPHP [ WE CAN DO IT JUST BeautySoft ]
// +----------------------------------------------------------------------
// | Copyright (c) 2012~2017 http://git.oschina.net/ceiba/CleverPHP All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: ceiba <ceiba_@126.com>
// +----------------------------------------------------------------------

//初始模型
if(!function_exists("M")){
	function M($model=""){
		if (!empty($GLOBALS["APP_NAME"])){
			$appname=$GLOBALS["APP_NAME"];
		}else{
			if (!defined("appname")){
				exit("应用名称为空");
			}
			$appname=appname;
		}
		$model=ucwords($model);
		if (!class_exists("PublicModel")){
			require_once $_SERVER["DOCUMENT_ROOT"] . '/'.$appname.'/Models/PublicModel.class.php';
		}
		$filed=$_SERVER['DOCUMENT_ROOT'] . '/'.$appname.'/Models/'.$model.'Model.class.php';
		$isFiel=file_exists($filed);
		$obj=null;
		if ($isFiel){
			include_once $filed;
			$m_model=ucwords($model)."Model";
			$obj=new $m_model;
			$obj->table_name=$GLOBALS["prefix"].strtolower($model);
		}else{
			$obj=new  BaseModel();
			$obj->table_name=@$GLOBALS["prefix"].strtolower($model);
			
		}
		return $obj;
	}
}

//加载扩展类
if(!function_exists("Vendor")){
	function Vendor($fileName="",$is_exclude_load=false){
		if ($is_exclude_load==false){
			$filed=dirname(__FILE__).'/../Librarys/Extend/Vendor/'.$fileName;
		}else{
			$projectName=appname;
			$projectName=explode("/",appname);
			$projectName=$projectName[0];
			$filed=$_SERVER['DOCUMENT_ROOT'] . '/'.$projectName."/Globals/_ExcludeLoad/".$fileName;
			
		}
		
		$isFiel=file_exists($filed);
		if ($isFiel){
			include_once $filed;
		}
		
	}
}
//加载驱动类
if(!function_exists("Drive")){
	function Drive($driveName=""){
		$filed=dirname(__FILE__).'/../Librarys/Extend/Drive/'.$driveName.".class.php";
		
		$isFiel=file_exists($filed);
		if ($isFiel){
			include_once $filed;
		}
		
	}
}
if(!function_exists("D")){
	function D($model=""){
		if (!empty($GLOBALS["APP_NAME"])){
			$appname=$GLOBALS["APP_NAME"];
		}else{
			$appname=appname;
		}
		$model=ucwords($model);
		$filed=$_SERVER['DOCUMENT_ROOT'] . '/'.$appname.'/Models/'.$model.'Model.class.php';
		
		$isFiel=file_exists($filed);
		$obj=null;
		if ($isFiel){
			include_once $filed;
			$m_model=ucwords($model)."Model";
			$obj=new $m_model;
			
		}
		return $obj;
	}
}
if(!function_exists("Using")){
	function Using($className,$appName="master"){
		$src=$_SERVER["DOCUMENT_ROOT"]."/";
		if (stripos(strtolower($className), "action")){
			$className=ucwords($className);
			$src.=$appName."/Actions/{$className}.class.php";
		}if (stripos(strtolower($className), "model")){
			$className=ucwords($className);
			$src.=$appName."/Models/{$className}.class.php";
		}
		require_once $src;
		
		
	}
}
//调用
function A($action_name){
	if (!empty($GLOBALS["APP_NAME"])){
		$appname=$GLOBALS["APP_NAME"];
	}else{
		$appname=appname;
	}
	$action_name=ucwords($action_name);
	if (!class_exists("BaseAction")){
		require($_SERVER["DOCUMENT_ROOT"] . '/'.$appname.'/Actions/BaseAction.class.php');
	}
	$filed=$_SERVER['DOCUMENT_ROOT'] . '/'.$appname.'/Actions/'.$action_name.'Action.class.php';
	$isFiel=file_exists($filed);
	$obj=null;
	if ($isFiel){
		include_once $filed;
		$action=ucwords($action_name)."Action";
		$obj=new $action;
	}else{
		$obj=new  BaseAction();
	}
	return $obj;
}
//获取配置文件
function C($filed="",$config_file="config"){
	$config1=array();
	$config2=array();
	$file=null;
	if (!empty($GLOBALS["APP_NAME"])){
		$appname=$GLOBALS["APP_NAME"];
	}else{
		$file=dirname(__FILE__).'/../Conf/config.php';
		$config1=include $file;
		if(defined("appname")){
			$appname=appname;
			$file=null;
			$file=$_SERVER['DOCUMENT_ROOT'] . '/'.$appname.'/Conf/'.$config_file.'.php';
			$isFiel=file_exists($file);
			if (!$isFiel) exit("配置文件不存在");
			$config2=include $file;
		}
		
	}
	if((is_array($config1)  && is_array($config2))){
		$config=array_merge($config1,$config2);
	}else{
		$config=$config1;
	}
	
	if(is_array($config) && $config){
		if (empty($filed)){
			return $config;
		}else{
			if (isset($config[$filed])){
				return $config[$filed];
			}else{
				return false;
			}
		}
	}
	return $file;
	
}
//是否手机
function is_mobile(){
	$regExp="/nokia|iphone|android|samsung|htc|motorola|blackberry|ericsson|huawei|dopod|amoi|gionee|^sie\-|^bird|^zte\-|haier|";
	$regExp.="blazer|netfront|helio|hosin|novarra|techfaith|palmsource|^mot\-|softbank|foma|docomo|kddi|up\.browser|up\.link|";
	$regExp.="symbian|smartphone|midp|wap|phone|windows ce|CoolPad|webos|iemobile|^spice|longcos|pantech|portalmmm|";
	$regExp.="alcatel|ktouch|nexian|^sam\-|s[cg]h|^lge|philips|sagem|wellcom|bunjalloo|maui|";
	$regExp.="jig\s browser|hiptop|ucweb|ucmobile|opera\s*mobi|opera\*mini|mqqbrowser|^benq|^lct";
	$regExp.="480×640|640x480|320x320|240x320|320x240|176x220|220x176/i";
	if(!isset($_SERVER['HTTP_USER_AGENT'])){
		return true;
	}else{
		return @$_GET['mobile'] || isset($_SERVER['HTTP_X_WAP_PROFILE']) || isset($_SERVER['HTTP_PROFILE']) || preg_match($regExp, strtolower($_SERVER['HTTP_USER_AGENT']));
	}
}
//初始自定义类
function new_class($class_name){
	$file=$_SERVER['DOCUMENT_ROOT'] . '/'.appname.'/Librarys/'.$class_name.'.php';
	$isFile=file_exists($file);
	if ($isFile){
		include_once $file;
	}else{
		error("找到文件：".$file);
	}
	$obj=new $class_name;
	return $obj;
}
function getFileList($directory) {
	$files = array();
	if(is_dir($directory)) {
		if($dh = opendir($directory)) {
			while(($file = readdir($dh)) !== false) {
				if($file != '.' && $file != '..') {
					$files[] = $file;
				}
			}
			closedir($dh);
		}
	}
	return $files;
}
//全局上传
function ajax_upload($dir="images",$return_type=1){
	if (!empty($_REQUEST["del"])){
		@unlink("../data/".$dir."/".$_REQUEST["del"]);
		exit();
	}
	if ($_GET["dom"]){
		echo '<script>document.domain = "'.$_GET["dom"].'";</script>';
	}
	$source_file = $_FILES["{$_REQUEST['fiel_name']}"]['tmp_name'];
	if (empty($source_file)){
		Util::JSON(0,"请选择上传文件",0);
	}
	$urlstr = date('Ymd');
	for ($i = 0; $i < 6; $i++)
	{
	$urlstr .= chr(mt_rand(97, 122));
	}
	//
	$target      = ROOT_PATH . DATA_DIR . '/'.$dir.'/';
	
	$s_name=null;
	if (stripos($_FILES["{$_REQUEST['fiel_name']}"]['name'],".")) {
		$s_name=explode(".",$_FILES["{$_REQUEST['fiel_name']}"]['name']);
		$s_name=$s_name[1];
	}
	$file_name   = $urlstr .'.'.$s_name;
	$w=$_REQUEST["w"];
	$h=$_REQUEST["h"];
	if ((strtolower($s_name)=="jpg" || strtolower($s_name)=="png" || strtolower($s_name)=="gif") && (empty($h) && empty($h))){
		$w=65;
		$h=65;
	}
	
	if (move_upload_file($source_file, $target.$file_name)) {
		if (!empty($w) && !empty($h)){
			$file_name=basename($target.$file_name);
			$tPath=str_replace(basename($file_name), "", $target.$file_name);
			$thume_name=$tPath."thumb_".basename($target.$file_name);
			Image::thumb2($target.$file_name,$thume_name,'',$w,$h,true);
			if ($w==65 && $h==65){
				$thume_name=$tPath."thumb_100_".basename($target.$file_name);
				Image::thumb2($target.$file_name,$thume_name,'',100,100,true);
				$thume_name=$tPath."thumb_150_".basename($target.$file_name);
				Image::thumb2($target.$file_name,$thume_name,'',150,150,true);
			}
		}
		if ($return_type==1){
			Util::JSON($file_name,$s_name,1);
		}
		return $file_name;
		
	}else{
		if ($return_type==1){
			Util::JSON(0,"上传失败".$target.$file_name,0);
		}
		return 0;
	}
	exit();

}
//过虑1维数组
function strip_psot_data($data){
	$resault=array();
	if ($data) {
		$keys=array_keys($data);
		foreach ($keys as $v){
			if ($data[$v]){
				@$resault[][trim(strip_tags($v))]=@trim(strip_tags($data[$v]));
			}else{
				@$resault[][trim(strip_tags($v))]="0";
			}
			
		}

	}
	$arr2=array();
	foreach ($resault as $k=>$v) {
		if (is_array($v)) {
			foreach ($v as $k2=>$v2) $arr2[$k2]=$v2;
		}
	}
	return $arr2;
}

//过虑1维数组，数字自动转为int
function strip_psot_data_number_convertoint($data){
	$resault=array();
	if ($data) {
		$keys=array_keys($data);
		foreach ($keys as $v){
			if ($data[$v]){
				$va=@trim(strip_tags($data[$v]));
				if (is_numeric($va)){
					if ($va<2147483647){
						$va=intval($va);
					}
				}
				@$resault[][trim(strip_tags($v))]=$va;
			}else{
				@$resault[][trim(strip_tags($v))]=0;
			}
				
		}

	}
	$arr2=array();
	foreach ($resault as $k=>$v) {
		if (is_array($v)) {
			foreach ($v as $k2=>$v2) $arr2[$k2]=$v2;
		}
	}
	return $arr2;
}
//过虑多维数组
function stripslashes_array(&$array) {
	while(list($key,$var) = each($array)) {
		if ($key != 'argc' && $key != 'argv' && (strtoupper($key) != $key || ''.intval($key) == "$key")) {
			if (is_string($var)) {
				$var=urldecode($var)?urldecode($var):$var;
				$array[$key] = trim(strip_tags(stripslashes($var)));
			}
			if (is_array($var)) {
				$array[$key] = stripslashes_array($var);
			}
		}
	}
	return $array;
}
/**
 * 快捷实例化模型
 * Enter description here ...
 * @param unknown_type $ModelName
 */
function MOD($ModelName=""){
	
		$filed=dirname(__FILE__).'/../Models/'.$ModelName.".class.php";
		$isFiel=file_exists($filed);
		
		if ($isFiel){
			include_once $filed;
		}
		$ModelName=ucfirst($ModelName);
		$ModelName=$ModelName."Model";
		if (class_exists($ModelName,false)){
			return new $ModelName();
		}
	
	
}
//捕获异常
function runException($e){
	echo $e->getMessage();
}

function RPC(string $service,array $post=array()):bool{
	if (stripos($service, ".")<3) return false;
	$arrays=explode(".", $service);
	$appname=$arrays[0];
	$className=$arrays[1];
	$functionName=$arrays[2];
	
	return true;
}
function CleverPHPLogs($logs,$projectName=null,$extend_logs=array(),$logName="logs"){
	date_default_timezone_set('PRC');
	$str = "\r\n-- ". date('Y-m-d H:i:s'). " --------------------------------------------------------------\r\n\n\n";
	$arg=$logs;
	if (is_array($arg))
	{
		
		
		$str .= '$arg = array(';
		foreach ($arg AS $val)
		{
			foreach ($val AS $key => $list)
			{
				$str .= "'$key' => '$list'\r\n\n\n";
			}
		}
		$str .= ")\r\n\n\n";
	}
	else
	{
		
		$str .= $arg;
	}
	$projectName=$projectName?$projectName:"";
	$logFile=$_SERVER['DOCUMENT_ROOT']."/".$projectName.'/Logs/'.$logName.'.txt';
	$logFile=str_replace("//", "/", $logFile);
	if (!file_exists($logFile)){
		file_put_contents($logFile, $str);
	}else{
		$fp=@fopen($logFile,'a+');
		if ($fp){
			fwrite($fp,$str);
		}
		fclose($fp);
	}
}


