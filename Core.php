<?php
// +----------------------------------------------------------------------
// | CleverPHP [ WE CAN DO IT JUST BeautySoft ]
// +----------------------------------------------------------------------
// | Copyright (c) 2012~2017 http://git.oschina.net/ceiba/CleverPHP All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: ceiba <ceiba_@126.com>
// +----------------------------------------------------------------------

class Core{
	static function run(){
		self::init();
		$mod=@ucwords($_GET["m"]);
		$action=@strtolower($_GET["a"]);
		if (empty($mod)) $mod="Index";
		if (empty($action)) $action="Index";
		if (!empty($GLOBALS["APP_NAME"])){
			$appname=$GLOBALS["APP_NAME"];
		}else{
			if (!defined("appname")){
				exit("未知的应用名称");
			}
			$appname=appname;
		}
		$filed=$_SERVER["DOCUMENT_ROOT"] . '/'.$appname.'/Actions/'.$mod."Action.class.php";
		
		$isFiel=file_exists($filed);
		
		if(!$isFiel){
			Vendor("CoreUtil.class.php");
			CoreUtil::includes($appname.'/Actions/');
		}else{
			include_once $filed;
		}
		
		$class_name=$mod."Action";
		if (!class_exists($class_name)){
			exit("你要找的栏目不存在");
			exit();
		}
		
		$comon_files=self::get_filenamesbydir($_SERVER["DOCUMENT_ROOT"] . '/'.$appname.'/Common/');
		//
		
		if ($comon_files){
			
			foreach ($comon_files as $key=>$value){
				if (stripos($value,".php")){
					if (file_exists($value)){
						require_once $value;
					}
				}
				
			}
		}
		
		
		
		$obj=new $class_name;
		if (method_exists($obj, $action)){
			$GLOBALS["model"]=strtolower($mod);
			$GLOBALS["action"]=strtolower($action);
			ob_end_clean();
			$obj->$action();
		}else{
			error("你要找的页面不存在");
			exit();
		}
	}
	static function get_filenamesbydir($dir){
		$files =  array();
		self::get_allfiles($dir,$files);
		return $files;
	}
	static function get_allfiles($path,&$files) {
		if(is_dir($path)){
			$dp = dir($path);
			while ($file = $dp ->read()){
				if($file !="." && $file !=".."){
					self::get_allfiles($path."".$file, $files);
				}
			}
			$dp ->close();
		}
		if(is_file($path)){
			$files[] =  $path;
		}
	}
	//初始化
	static function init(){
		$path=pathinfo(__FILE__);
		$dir=$path["dirname"];
		require_once $dir.'/Action/Action.class.php';
		require_once $dir.'/Models/BaseModel.class.php';
		require_once $dir.'/Common/common.php';
		require_once $dir.'/Librarys/Image.class.php';
		require_once $dir.'/Librarys/Pager.php';
		require_once $dir.'/Librarys/Cache.class.php';
		require_once $dir.'/Librarys/Obj.php';
		require_once $dir.'/Librarys/Rpc.class.php';
		require_once $dir.'/Librarys/PDO_DB_Conn.class.php';
		require_once $dir.'/Models/Model.class.php';
		require_once $dir.'/View/View.class.php';
		
		
	}
	
	
}
class Objects{
	
}

?>